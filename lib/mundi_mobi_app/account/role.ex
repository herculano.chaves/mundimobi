defmodule MundiMobiApp.Account.Role do
  use Ecto.Schema
  import Ecto.Changeset

  schema "roles" do
    field :permissions, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(role, attrs) do
    role
    |> cast(attrs, [:title, :permissions])
    |> validate_required([:title, :permissions])
  end
end
