defmodule MundiMobiApp.Account.Address do
  use Ecto.Schema
  import Ecto.Changeset

  schema "addresses" do
    field :complement, :string
    field :district_id, :integer
    field :lat, :string
    field :locale, :string
    field :lon, :string
    field :model, :string
    field :model_id, :integer
    field :number, :integer

    timestamps()
  end

  @doc false
  def changeset(address, attrs) do
    address
    |> cast(attrs, [:model, :model_id, :locale, :number, :complement, :lat, :lon, :district_id])
    |> validate_required([:model, :model_id, :locale, :number, :complement, :lat, :lon, :district_id])
  end
end
