defmodule MundiMobiApp.Account.Profile do
  use Ecto.Schema
  import Ecto.Changeset

  schema "profiles" do
    field :age, :integer
    field :birthday, :date
    field :doc_number, :string
    field :sex, :string
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(profile, attrs) do
    profile
    |> cast(attrs, [:user_id, :doc_number, :sex, :age, :birthday])
    |> validate_required([:user_id, :doc_number, :sex, :age, :birthday])
  end
end
