defmodule MundiMobiApp.Account.User.Role do
  use Ecto.Schema
  import Ecto.Changeset

  schema "user_roles" do
    field :ser_id, :id
    field :role_id, :id

    timestamps()
  end

  @doc false
  def changeset(role, attrs) do
    role
    |> cast(attrs, [])
    |> validate_required([])
  end
end
