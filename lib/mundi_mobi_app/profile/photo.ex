defmodule MundiMobiApp.Profile.Photo do
  use Ecto.Schema
  import Ecto.Changeset

  schema "user_photos" do
    field :medium, :string
    field :normal, :string
    field :thumbnail, :string
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(photo, attrs) do
    photo
    |> cast(attrs, [:normal, :medium, :thumbnail])
    |> validate_required([:normal, :medium, :thumbnail])
  end
end
