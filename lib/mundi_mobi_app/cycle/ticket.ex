defmodule MundiMobiApp.Cycle.Ticket do
  use Ecto.Schema
  import Ecto.Changeset

  schema "tickets" do
    field :expire_date, :naive_datetime
    field :init_date, :naive_datetime
    field :is_subscription, :boolean, default: false
    field :paid_price, :float
    field :status, :integer
    field :user_id, :id
    field :event_id, :id
    field :seat_id, :id
    field :subscription_id, :id

    timestamps()
  end

  @doc false
  def changeset(ticket, attrs) do
    ticket
    |> cast(attrs, [:paid_price, :is_subscription, :init_date, :expire_date, :status])
    |> validate_required([:paid_price, :is_subscription, :init_date, :expire_date, :status])
  end
end
