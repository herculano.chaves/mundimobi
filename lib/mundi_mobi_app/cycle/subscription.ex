defmodule MundiMobiApp.Cycle.Subscription do
  use Ecto.Schema
  import Ecto.Changeset

  schema "subscriptions" do
    field :status, :integer
    field :plan_id, :id
    field :customer_id, :id

    timestamps()
  end

  @doc false
  def changeset(subscription, attrs) do
    subscription
    |> cast(attrs, [:status])
    |> validate_required([:status])
  end
end
