defmodule MundiMobiApp.Cycle.Passport do
  use Ecto.Schema
  import Ecto.Changeset

  schema "_passports" do
    field :cover_photo, :string
    field :event_id, :id
    field :ticket_id, :id

    timestamps()
  end

  @doc false
  def changeset(passport, attrs) do
    passport
    |> cast(attrs, [:cover_photo])
    |> validate_required([:cover_photo])
  end
end
