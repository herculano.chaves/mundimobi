defmodule MundiMobiApp.Cycle.Plan do
  use Ecto.Schema
  import Ecto.Changeset

  schema "plans" do
    field :active, :boolean, default: false
    field :cut_date, :naive_datetime
    field :name, :string
    field :price, :float
    field :valid, :boolean, default: false
    field :customer_id, :id

    timestamps()
  end

  @doc false
  def changeset(plan, attrs) do
    plan
    |> cast(attrs, [:name, :price, :cut_date, :valid, :active])
    |> validate_required([:name, :price, :cut_date, :valid, :active])
  end
end
