defmodule MundiMobiApp.Repo do
  use Ecto.Repo,
    otp_app: :mundi_mobi_app,
    adapter: Ecto.Adapters.MyXQL
end
