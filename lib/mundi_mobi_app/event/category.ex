defmodule MundiMobiApp.Event.Category do
  use Ecto.Schema
  import Ecto.Changeset

  schema "event_categories" do
    field :description, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [:name, :description])
    |> validate_required([:name, :description])
  end
end
