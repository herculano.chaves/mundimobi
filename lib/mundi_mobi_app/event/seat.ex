defmodule MundiMobiApp.Event.Seat do
  use Ecto.Schema
  import Ecto.Changeset

  schema "event_seats" do
    field :discount, :float
    field :discount_percentage, :integer
    field :price, :float
    field :event_id, :id
    field :seat_region_id, :id

    timestamps()
  end

  @doc false
  def changeset(seat, attrs) do
    seat
    |> cast(attrs, [:price, :discount, :discount_percentage])
    |> validate_required([:price, :discount, :discount_percentage])
  end
end
