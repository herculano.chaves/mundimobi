defmodule MundiMobiApp.Event.Event do
  use Ecto.Schema
  import Ecto.Changeset

  schema "events" do
    field :description, :string
    field :discount, :float
    field :discount_percentage, :integer
    field :name, :string
    field :customer_id, :id
    field :category_id, :id
    field :address_id, :id

    timestamps()
  end

  @doc false
  def changeset(event, attrs) do
    event
    |> cast(attrs, [:name, :description, :discount, :discount_percentage])
    |> validate_required([:name, :description, :discount, :discount_percentage])
  end
end
