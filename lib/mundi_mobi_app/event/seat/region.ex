defmodule MundiMobiApp.Event.Seat.Region do
  use Ecto.Schema
  import Ecto.Changeset

  schema "event_regions" do
    field :discount, :float
    field :discount_percentage, :integer
    field :event_id, :id

    timestamps()
  end

  @doc false
  def changeset(region, attrs) do
    region
    |> cast(attrs, [:discount, :discount_percentage])
    |> validate_required([:discount, :discount_percentage])
  end
end
