defmodule MundiMobiAppWeb.UserView do
  use MundiMobiAppWeb, :view
  alias MundiMobiAppWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("index_api.json", %{users: users}) do
    %{data: render_many(users, UserView, "user_api.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{id: user.id,
      name: user.name,
      email: user.email,
      password: user.password,
      session_token: user.session_token,
      address_id: user.address_id}
  end

  def render("user_api.json", %{user: user}) do
    %{name: user.name,
      email: user.email}
  end
end
