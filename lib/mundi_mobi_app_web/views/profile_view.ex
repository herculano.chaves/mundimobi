defmodule MundiMobiAppWeb.ProfileView do
  use MundiMobiAppWeb, :view
  alias MundiMobiAppWeb.ProfileView

  def render("index.json", %{profiles: profiles}) do
    %{data: render_many(profiles, ProfileView, "profile.json")}
  end

  def render("show.json", %{profile: profile}) do
    %{data: render_one(profile, ProfileView, "profile.json")}
  end

  def render("profile.json", %{profile: profile}) do
    %{id: profile.id,
      user_id: profile.user_id,
      doc_number: profile.doc_number,
      sex: profile.sex,
      age: profile.age,
      birthday: profile.birthday}
  end
end
