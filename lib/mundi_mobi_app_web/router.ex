defmodule MundiMobiAppWeb.Router do
  use MundiMobiAppWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", MundiMobiAppWeb do
    pipe_through :api

    resources "/users", UserController, except: [:new, :edit]
    resources "/profiles", ProfileController, except: [:new, :edit]

    get "/only", UserController, :api_index
  end
end
