use Mix.Config

# Configure your database
config :mundi_mobi_app, MundiMobiApp.Repo,
  username: "developer",
  password: "1234qwer",
  database: "mundi_mobi_app_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :mundi_mobi_app, MundiMobiAppWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
