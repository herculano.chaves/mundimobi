defmodule MundiMobiApp.Repo.Migrations.DropUserRoles do
  use Ecto.Migration

  def change do

    drop table(:user_roles)

  end
end
