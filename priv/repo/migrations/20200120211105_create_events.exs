defmodule MundiMobiApp.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :name, :string
      add :description, :string
      add :discount, :float
      add :discount_percentage, :integer
      add :customer_id, references(:customers, on_delete: :nothing)
      add :category_id, references(:event_categories, on_delete: :nothing)
      add :address_id, references(:addresses, on_delete: :nothing)

      timestamps()
    end

    create index(:events, [:customer_id])
    create index(:events, [:category_id])
    create index(:events, [:address_id])
  end
end
