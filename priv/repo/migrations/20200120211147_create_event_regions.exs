defmodule MundiMobiApp.Repo.Migrations.CreateEventRegions do
  use Ecto.Migration

  def change do
    create table(:event_regions) do
      add :discount, :float
      add :discount_percentage, :integer
      add :event_id, references(:events, on_delete: :nothing)

      timestamps()
    end

    create index(:event_regions, [:event_id])
  end
end
