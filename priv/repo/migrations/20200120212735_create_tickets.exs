defmodule MundiMobiApp.Repo.Migrations.CreateTickets do
  use Ecto.Migration

  def change do
    create table(:tickets) do
      add :paid_price, :float
      add :is_subscription, :boolean, default: false, null: false
      add :init_date, :naive_datetime
      add :expire_date, :naive_datetime
      add :status, :integer
      add :user_id, references(:users, on_delete: :nothing)
      add :event_id, references(:events, on_delete: :nothing)
      add :seat_id, references(:event_seats, on_delete: :nothing)
      add :subscription_id, references(:subscriptions, on_delete: :nothing)

      timestamps()
    end

    create index(:tickets, [:user_id])
    create index(:tickets, [:event_id])
    create index(:tickets, [:seat_id])
    create index(:tickets, [:subscription_id])
  end
end
