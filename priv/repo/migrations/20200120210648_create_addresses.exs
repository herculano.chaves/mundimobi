defmodule MundiMobiApp.Repo.Migrations.CreateAddresses do
  use Ecto.Migration

  def change do
    create table(:addresses) do
      add :model, :string
      add :model_id, :integer
      add :locale, :string
      add :number, :integer
      add :complement, :string
      add :lat, :string
      add :lon, :string
      add :district_id, :integer

      timestamps()
    end

  end
end
