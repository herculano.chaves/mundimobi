defmodule MundiMobiApp.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :email, :string
      add :password, :string
      add :session_token, :string
      add :address_id, :integer

      timestamps()
    end

  end
end
