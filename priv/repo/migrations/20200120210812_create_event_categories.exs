defmodule MundiMobiApp.Repo.Migrations.CreateEventCategories do
  use Ecto.Migration

  def change do
    create table(:event_categories) do
      add :name, :string
      add :description, :string

      timestamps()
    end

  end
end
