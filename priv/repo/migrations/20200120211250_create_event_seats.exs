defmodule MundiMobiApp.Repo.Migrations.CreateEventSeats do
  use Ecto.Migration

  def change do
    create table(:event_seats) do
      add :price, :float
      add :discount, :float
      add :discount_percentage, :integer
      add :event_id, references(:events, on_delete: :nothing)
      add :seat_region_id, references(:event_regions, on_delete: :nothing)

      timestamps()
    end

    create index(:event_seats, [:event_id])
    create index(:event_seats, [:seat_region_id])
  end
end
