defmodule MundiMobiApp.Repo.Migrations.CreateUserPhotos do
  use Ecto.Migration

  def change do
    create table(:user_photos) do
      add :normal, :string
      add :medium, :string
      add :thumbnail, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:user_photos, [:user_id])
  end
end
