defmodule MundiMobiApp.Repo.Migrations.CreateSubscriptions do
  use Ecto.Migration

  def change do
    create table(:subscriptions) do
      add :status, :integer
      add :plan_id, references(:plans, on_delete: :nothing)
      add :customer_id, references(:customers, on_delete: :nothing)

      timestamps()
    end

    create index(:subscriptions, [:plan_id])
    create index(:subscriptions, [:customer_id])
  end
end
