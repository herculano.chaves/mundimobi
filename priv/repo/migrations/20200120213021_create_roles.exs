defmodule MundiMobiApp.Repo.Migrations.CreateRoles do
  use Ecto.Migration

  def change do
    create table(:roles) do
      add :title, :string
      add :permissions, :string

      timestamps()
    end

  end
end
