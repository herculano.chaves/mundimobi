defmodule MundiMobiApp.Repo.Migrations.CreatePlans do
  use Ecto.Migration

  def change do
    create table(:plans) do
      add :name, :string
      add :price, :float
      add :cut_date, :naive_datetime
      add :valid, :boolean, default: false, null: false
      add :active, :boolean, default: false, null: false
      add :customer_id, references(:customers, on_delete: :nothing)

      timestamps()
    end

    create index(:plans, [:customer_id])
  end
end
