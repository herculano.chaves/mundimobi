defmodule MundiMobiApp.Repo.Migrations.CreateProfiles do
  use Ecto.Migration

  def change do
    create table(:profiles) do
      add :user_id, :integer
      add :doc_number, :string
      add :sex, :string
      add :age, :integer
      add :birthday, :date

      timestamps()
    end

  end
end
