defmodule MundiMobiApp.Repo.Migrations.CreateCustomers do
  use Ecto.Migration

  def change do
    create table(:customers) do
      add :name, :string
      add :contact_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:customers, [:contact_id])
  end
end
