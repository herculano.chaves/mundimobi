defmodule MundiMobiApp.Repo.Migrations.CreatePassports do
  use Ecto.Migration

  def change do
    create table(:_passports) do
      add :cover_photo, :string
      add :event_id, references(:events, on_delete: :nothing)
      add :ticket_id, references(:tickets, on_delete: :nothing)

      timestamps()
    end

    create index(:_passports, [:event_id])
    create index(:_passports, [:ticket_id])
  end
end
